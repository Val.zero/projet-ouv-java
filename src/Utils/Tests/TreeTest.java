package Utils.Tests;

import Utils.Tree;
import org.junit.jupiter.api.Test;

import java.util.ArrayList;
import java.util.Arrays;

import static org.junit.jupiter.api.Assertions.*;

class TreeTest {

    @Test
    void insert() {
        Tree t = new Tree(5);
        t.insert(2);
        t.insert(8);
        t.insert(3);
        t.insert(6);
        assertEquals(t.getLeft().getRight().getLabel(), 3);
        assertEquals(t.getRight().getLeft().getLabel(), 6);
    }

    @Test
    void fromList() {
        Tree t = Tree.fromList(Arrays.asList(4, 2, 3, 8, 1, 9, 6, 7, 5));
        assertEquals(t.getLeft().getRight().getLabel(), 3);
        assertEquals(t.getRight().getLeft().getLeft().getLabel(), 5);
    }

    @Test
    void signature() {
        Tree t = Tree.fromList(Arrays.asList(4, 2, 3, 8, 1, 9, 6, 7, 5));
        assertEquals(t.signature(), "((())())((())())()");
    }

    @Test
    void prefixe() {
        Tree t = Tree.fromList(Arrays.asList(4, 2, 3, 8, 1, 9, 6, 7, 5));
        assertEquals(t.prefixe(), new ArrayList<>(Arrays.asList(4, 2, 1, 3, 8, 6, 5, 7, 9)));
    }

    @Test
    void compress() {
        Tree t = Tree.fromList(Arrays.asList(4, 2, 3, 8, 1, 9, 6, 7, 5));
        t.compress();
        Tree ll = t.getLeft().getLeft();
        Tree lr = t.getLeft().getRight();
        Tree rl = t.getRight().getLeft();
        Tree rr = t.getRight().getRight();
        assertEquals(ll.getType(), Tree.TreeType.NODE);
        assertEquals(lr.getType(), Tree.TreeType.CNODE);
        assertEquals(rl.getType(), Tree.TreeType.CNODE);
        assertEquals(rr.getType(), Tree.TreeType.CNODE);
    }

    @Test
    void search() {
        Tree t = Tree.fromList(Arrays.asList(4, 2, 3, 8, 1, 9, 6, 7, 5));
        assertTrue(t.search(5));
        assertFalse(t.search(10));
        t.compress();
        assertTrue(t.search(7));
        assertFalse(t.search(11));
    }
}