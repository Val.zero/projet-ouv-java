package Utils.Tests;

import Utils.ListShuffle;
import org.junit.jupiter.api.Test;

import java.util.List;

import static org.junit.jupiter.api.Assertions.assertEquals;
import static org.junit.jupiter.api.Assertions.assertTrue;

class ListShuffleTest {

    @Test
    void test_gen_permutation() {
        List<Integer> perm = ListShuffle.gen_permutation(8);
        assertEquals(perm.size(), 8);
        assertTrue(perm.contains(1));
        assertTrue(perm.contains(4));
        assertTrue(perm.contains(8));
    }

    @Test
    void test_gen_permutation2() {
        List<Integer> perm = ListShuffle.gen_permutation2(1, 8);
        assertEquals(perm.size(), 8);
        assertTrue(perm.contains(1));
        assertTrue(perm.contains(4));
        assertTrue(perm.contains(8));
    }
}
