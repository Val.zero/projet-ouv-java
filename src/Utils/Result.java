package Utils;

/***
 * A class used to wrap a value and mark it either as Ok or Error.
 * @param <T> the value's type
 */
class Result<T> {

    public enum Type{
        OK, ERR
    }

    private final Type type;
    private final T value;

    public Result(Type type, T value) {
        this.type = type;
        this.value = value;
    }

    public Type getType() {
        return type;
    }

    public T getValue() {
        return value;
    }
}
