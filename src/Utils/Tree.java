package Utils;

import java.util.ArrayList;
import java.util.List;

/***
 * A class representing a tree and its operations, most notably compression.
 */
public class Tree {

    /***
     * A tree can either be a leaf, a node or a compressed node.
     */
    public enum TreeType {
        LEAF, NODE, CNODE
    }

    private TreeType type;

    /* Nodes only */
    private int label;
    private Tree left;
    private Tree right;
    private int size;
    private boolean lock;

    /* CNodes only */
    private ArrayList<Integer> tab;
    private Tree refTree;

    /***
     * Makes a new node with leaves as left and right subtrees.
     * @param label the integer label of the root
     */
    public Tree(int label) {
        this.type = TreeType.NODE;
        this.label = label;
        this.left = new Tree();
        this.right = new Tree();
        this.size = 1;
        this.lock = false;
    }

    /***
     * Makes a new leaf.
     */
    private Tree() {
        this.type = TreeType.LEAF;
    }

    public TreeType getType() {
        return this.type;
    }

    public int getLabel() {
        return this.label;
    }

    public Tree getLeft() {
        return left;
    }

    public Tree getRight() {
        return right;
    }

    public int getSize() {
        return size;
    }

    public void lock() {
        this.lock = true;
    }

    public Tree getRefTree() {
        return refTree;
    }

    /***
     * Inserts an integers in the tree.
     * @param n the integer to insert
     */
    public boolean insert(int n) {
        boolean res = false;
        switch (type){
            case LEAF: {
                this.type = TreeType.NODE;
                this.label = n;
                this.left = new Tree();
                this.right = new Tree();
                this.size++;
                this.lock = false;
                res = true;
                break;
            }
            case NODE: {
                if (lock) {
                    break;
                }
                switch (Integer.compare(n, this.label)) {
                    case 0:{
                        break;
                    }
                    case 1:{
                        res = right.insert(n);
                        if (res) size ++;
                        break;
                    }
                    default: {
                        res = left.insert(n);
                        if (res) size++;
                        break;
                    }
                }
            }
            case CNODE:{
                break;
            }
        }
        return res;
    }

    /***
     * Makes a tree containing the elements of a list, inserted in order.
     * @param l an Tnteger List
     * @return the root of the new tree, with the first element of the list as label
     */
    public static Tree fromList(List<Integer> l){
        Tree res = new Tree();
        for (Integer i: l){
            res.insert(i);
        }
        return res;
    }

    /***
     * Returns the tree's signature, which represents its structure.
     * With l as the left subtree and r as the right one, the signature is :
     * - empty if the tree is a leaf
     * - "(" l.signature() ")" r.signature() otherwise
     * @return a string containing the tree's signature
     */
    public String signature() {
        String res;
        switch (type){
            case NODE:{
                res = "(" + this.left.signature() + ")" + this.right.signature();
                break;
            }
            case CNODE:{
                res = refTree.signature();
                break;
            }
            default:{
                res = "";
            }
        }
        return res;
    }

    /***
     * Returns the elements of the tree in prefix order.
     * @return a list of the tree's elements.
     */
    public ArrayList<Integer> prefixe() {
        ArrayList<Integer> res = new ArrayList<>();
        switch (type){
            case NODE:{
                res.add(label);
                res.addAll(left.prefixe());
                res.addAll(right.prefixe());
                break;
            }
            case CNODE:{
                return tab;
            }
        }
        return res;
    }

    /***
     * A commodity method to create Result<Tree>.
     * @param t the tree to wrap
     * @return an Ok result wrapping the tree
     */
    private Result<Tree> Ok(Tree t){
        return new Result<>(Result.Type.OK, t);
    }

    /***
     * A commodity method to create Result<Tree>.
     * @param t the tree to wrap
     * @return an Err result wrapping the tree
     */
    private Result<Tree> Err(Tree t){
        return new Result<>(Result.Type.ERR, t);
    }

    //TODO: Can we get rid of Result simply by testing the tree's type eache time ?
    /***
     * Auxiliary function for compress.
     * @param model the tree which we compare this tree's structure with
     * @return this tree, either compressed or with its subtrees compressed
     */
    private Result<Tree> compressAux(Tree model){
        Result<Tree> res;
        switch (type){
            case NODE:{
                if (this.signature().equals(model.signature())){
                    this.tab = this.prefixe();
                    this.refTree = model;
                    model.lock();
                    this.left = null;
                    this.right = null;
                    this.type = TreeType.CNODE;
                    res = Ok(this);
                } else {
                    switch (model.getType()){
                        case NODE:{
                            Result<Tree> tmp = this.compressAux(model.getLeft());
                            if (tmp.getType() == Result.Type.OK) {
                                res = tmp;
                            } else {
                                tmp = this.compressAux(model.getRight());
                                if (tmp.getType() == Result.Type.OK) {
                                    res = tmp;
                                } else {
                                    this.left = left.compressAux(model).getValue();
                                    this.right = right.compressAux(model).getValue();
                                    res = Err(this);
                                }
                            }
                            break;
                        }
                        case CNODE:{
                            res = this.compressAux(model.getRefTree());
                            break;
                        }
                        default:{
                            res = Err(this);
                        }
                    }
                }
                break;
            }
            case CNODE:{
                res = Ok(this);
                break;
            }
            default:{
                res = Err(this);
            }
        }
        return res;
    }

    /***
     * Compresses the tree by comparing subtrees structure.
     */
    public void compress(){
        if (type == TreeType.NODE) {
            left.compress();
            Result<Tree> tmp = right.compressAux(left);
            this.right = tmp.getValue();
            this.lock = (tmp.getType() == Result.Type.OK);
        }
    }

    /***
     * Auxiliary function for search, dealing with compressed nodes.
     * @param n the integer we are looking for
     * @param tab the labels of the CNode and its subtrees
     * @param i a counter
     * @return true if n is in the tree, false otherwise
     */
    private boolean searchAux(int n, ArrayList<Integer> tab, int i){
        boolean res;
        switch (type){
            case NODE:{
                switch (Integer.compare(n, tab.get(i))){
                    case 0:{
                        res = true;
                        break;
                    }
                    case 1:{
                        res = right.searchAux(n, tab, (i + left.getSize() + 1));
                        break;
                    }
                    default:{
                        res = left.searchAux(n, tab, (i + 1));
                    }
                }
                break;
            }
            case CNODE:{
                res = refTree.searchAux(n, tab, i);
                break;
            }
            default:{
                res = false;
            }
        }
        return res;
    }

    /***
     * Looks for an integer in the tree.
     * @param n the integer we are looking for
     * @return true if n is in the tree, false otherwise
     */
    public boolean search(int n){
        boolean res;
        switch (type){
            case NODE:{
                switch (Integer.compare(n, label)){
                    case 0:{
                        res = true;
                        break;
                    }
                    case 1:{
                        res = right.search(n);
                        break;
                    }
                    default:{
                        res = left.search(n);
                    }
                }
                break;
            }
            case CNODE:{
                res = refTree.searchAux(n, tab, 0);
                break;
            }
            default:{
                res = false;
            }
        }
        return res;
    }
}
