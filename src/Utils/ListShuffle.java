package Utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.LinkedList;
import java.util.Random;

/***
 * An utility class for generating random lists of integers.
 */
public class ListShuffle {
    private static final Random r = new Random();

    /***
     * Takes a random element from l1 and pushes it in l1.
     * @param l1 a list of Integer
     * @param l2 a list of Integer
     * @return the couple (l1, l2) with a random element removed from l1 and added to l2
     */
    private static ArrayList<ArrayList<Integer>> extraction_alea(ArrayList<Integer> l1, ArrayList<Integer> l2) {
        int l = l1.size();
        if (l != 0) {
            int i = r.nextInt(l);
            l2.add(l1.remove(i));
        }
        ArrayList<ArrayList<Integer>> res = new ArrayList<>();
        res.add(l1);
        res.add(l2);
        return res;
    }

    /***
     * Generates a random permutation of integers from 1 to n (included).
     * @param n an integer
     * @return a list containing the n first integers in random order
     */
    public static ArrayList<Integer> gen_permutation(int n) {
        ArrayList<Integer> l = new ArrayList<>();
        for(int i = 1; i <= n; i++) {
            l.add(i);
        }
        ArrayList<Integer> p = new ArrayList<>();
        for(int i = 0; i < n; i++) {
            ArrayList<ArrayList<Integer>> tmp = extraction_alea(l, p);
            l = tmp.get(0);
            p = tmp.get(1);
        }
        return p;
    }

    /***
     * Auxiliary function for intercale.
     * @param l1 a list of Integer
     * @param l2 a list of Integer
     * @param s1 the size of l1
     * @param s2 the size of l2
     * @param i a counter
     * @return a list containing l1's and l2's elements in random order
     */
    private static LinkedList<Integer> intercaleAux(LinkedList<Integer> l1, LinkedList<Integer> l2, int s1, int s2, int i) {
        if ((s1 == 0) || (i == s2)) {
            l2.addAll(l1);
            return l2;
        } else {
            double threshold = (double) s1 / (s1 + s2);
            if (r.nextDouble() < threshold) {
                l2.add(i, l1.removeFirst());
                return intercaleAux(l1, l2, s1 - 1, s2 + 1, i + 1);
            } else {
                return  intercaleAux(l1, l2, s1, s2, i + 1);
            }
        }
    }

    /***
     * Inserts elements of l1 in l2 at random intervals.
     * @param l1 a list of Integer
     * @param l2 a list of Integer
     * @return a list containing l1's and l2's elements in random order
     */
    private static LinkedList<Integer> intercale(LinkedList<Integer> l1, LinkedList<Integer> l2) {
        return intercaleAux(l1, l2, l1.size(), l2.size(), 0);
    }

    /***
     * Generates a random permutation of integers from p to q (included).
     * @param p an integer
     * @param q an integer
     * @return a list containing the integers from p to q in random order
     */
    public static LinkedList<Integer> gen_permutation2(int p, int q) {
        LinkedList<Integer> res;
        if (p > q) {
            res = new LinkedList<>();
        } else if (p == q) {
            res = new LinkedList<>(Collections.singletonList(p)) ;
        } else {
            res = intercale(gen_permutation2(p, ((p + q) / 2)), gen_permutation2((((p + q) / 2) + 1), q));
        }
        return res;
    }
}
