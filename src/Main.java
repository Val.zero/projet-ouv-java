import Utils.ListShuffle;
import Utils.Tree;
import org.apache.commons.cli.*;

import java.time.Instant;
import java.time.temporal.ChronoUnit;
import java.util.List;

public class Main {

    public static void main(String[] args) {
        Options options = new Options();
        options.addOption("g", "gen_perm", false, "Uses gen_permutation instead of gen_perumation2");
        options.addOption("N", "no_uncompressed", false, "Disables execution on uncompressed tree");
        options.addOption("n", "no_compressed", false, "Disables execution on compressed tree");
        options.addOption("s", "size", true, "Sets the size of the list to build the tree from");
        CommandLineParser parser = new DefaultParser();
        CommandLine cmd = null;
        try {
            cmd = parser.parse(options, args);
        } catch (ParseException e) {
            e.printStackTrace();
        }
        assert cmd != null;

        print_title();
        int size = 100;
        if (cmd.hasOption("s")) {
            size = Integer.parseInt(cmd.getOptionValue("s"));
        }
        List<Integer> searchlist = ListShuffle.gen_permutation2(1, 10);
        List<Integer> list;
        if (cmd.hasOption("g")) {
            list = timed_gen_perm(size);
        } else {
            list = timed_gen_perm2(size);
        }
        Tree t = Tree.fromList(list);
        // size
        if (!cmd.hasOption("N")) {
            double avg1 = perform_search(searchlist, t);
            System.out.println("Average searching time in uncompressed tree: " + avg1 + "ms\n");
            System.out.flush();
        }
        if (!cmd.hasOption("n")) {
            timed_compress(t);
            double avg2 = perform_search(searchlist, t);
            System.out.println("Average searching time in compressed tree: " + avg2 + "ms\n");
            System.out.flush();
        }
        System.out.println("End of program.");
    }

    private static void print_title() {
        System.out.println("==================================================");
        System.out.println("                   Java program");
        System.out.println("==================================================\n");
        System.out.flush();
    }

    private static List<Integer> timed_gen_perm(int n) {
        System.out.println("Using gen_permutation");
        Instant t = Instant.now();
        List<Integer> res = ListShuffle.gen_permutation(n);
        long dt = ChronoUnit.MILLIS.between(t, Instant.now());
        System.out.println("Permutation generation time: " + dt + "ms\n");
        System.out.flush();
        return res;
    }

    private static List<Integer> timed_gen_perm2(int n) {
        System.out.println("Using gen_permutation");
        Instant t = Instant.now();
        List<Integer> res = ListShuffle.gen_permutation2(1, n);
        long dt = ChronoUnit.MILLIS.between(t, Instant.now());
        System.out.println("Permutation generation time: " + dt + "ms\n");
        System.out.flush();
        return res;
    }

    private static long timed_search(int n, Tree tree) {
        Instant t = Instant.now();
        tree.search(n);
        // System.out.println("Searching time: " + res + "ms\n");
        return ChronoUnit.MILLIS.between(t, Instant.now());
    }

    private static double perform_search(List<Integer> searchlist, Tree tree) {
        double res = 0;
        for(int i: searchlist) {
            res += timed_search(i, tree);
        }
        return (res / searchlist.size());
    }

    private static void timed_compress(Tree tree) {
        Instant t = Instant.now();
        tree.compress();
        long dt = ChronoUnit.MILLIS.between(t, Instant.now());
        System.out.println("Comression time: " + dt + "ms");
        System.out.flush();
    }
}
